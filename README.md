# Redux vs Redux-Toolkit

This is a comparison of Redux-Toolkit with the old way to use Redux.
Using Redux-Toolkit can help reduce a lot of boilerplate from react.
Let's learn with a simple example of Todo list where you can add and remove items.

Let's jump right into it:
We first start with how we create a store. This doesn't look like much is different. By default Redux-Toolkit have 
middleWare initialize but we can also configure it ourselves simply by adding middleware to configureStore:

```node
// Before:
const store = createStore(todo)
// After:
const store = configureStore({
  reducer: todo,
  middleware: Saga
})
```

Writing Action Creators becomes easy, Redux-Toolkit provides a function createAction Which does the job. But further we will see using Redux-Toolkit we don't need to do this either:

```node
const addNewItem = createAction('ADD_NEW_ITEM')
```

Before we needed to create switch case for every action which are expected:
```node
const todo = (state = [], action) => {
  switch (action.type) {
    case ADD_NEW_ITEM:
      return state.push(action.payload)
    case REMOVE_ITEM:
      return state.splice(action.payload.id, 1)
    default:
      return state
  }
}
```

Redux-Toolkit Provides a function createReducer Which makes the job easier:
```node
const todo = createReducer(0, {
  addNewItem: (state, action) => state.push(action.payload),
  decrement: (state, action) => state.splice(action.payload.id, 1)
})
```

But it gets better as i mentioned earlier Redux-Toolkit provides a function createSlice, it eliminates the need to write action creators:
```node
const todoSlice = createSlice({
  name: 'todo',
  initialState: [],
  reducers: {
    addNewItem: (state, action) => state.push(action.payload),
    removeItem: (state, action) => state.splice(action.payload.id, 1)
  }
})
```
The advantage of using createSlice is that we don't need to write action creator, createSlice Does it for us, we can simply use ES6 destructuring syntax to pull out the action creator functions as variables, and possibly the reducer as well:

```node
const { actions, reducer } = todoSlice;

export const { addNewItem, removeItem } = actions;
```
We can use these action creators and call them wherever they are needed. The data passed with the function call is added to the action's payload and can be excessed with the action.payload.

### Conclusion 
Using Redux-Toolkit reduces a lot of boilerplate.You don’t need to write action creators and reducers separately, using Redux-Toolkit you can create and configure Redux store very easily and very rapidly.