//ReduxToolkit
import { createSlice } from 'react';

const todoSlice = createSlice({
  name: 'todo',
  initialState: 0,
  reducers: {
    addNewItem: (state, action) => state + action.payload,
    removeItem: (state, action) => state - action.payload.id
  }
})
const { actions, reducer } = todoSlice;
export const { addNewItem, removeItem } = actions;

const store = configureStore({
  reducer: reducer
})