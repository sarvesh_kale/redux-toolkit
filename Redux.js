//Old way to use Redux
const ADD_NEW_ITEM = 'ADD_NEW_ITEM'
const REMOVE_ITEM = 'REMOVE_ITEM'

const addNewItem = (value) => {
  return { type: ADD_NEW_ITEM, payload: value }
}
const removeItem = (id) => {
  return { type: REMOVE_ITEM, payload: id }
}

const todo = (state = 0, action) => {
  switch (action.type) {
    case ADD_NEW_ITEM:
      return state + action.payload
    case REMOVE_ITEM:
      return state - action.payload.id
    default:
      return state
  }
}

const store = createStore(todo)
